<?php

namespace Drupal\mercury_editor\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The typed config service.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager
  ) {
    parent::__construct($config_factory);
    $this->typedConfigManager = $typedConfigManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mercury_editor_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mercury_editor.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('mercury_editor.settings');
    $config_schema = $this->typedConfigManager->getDefinition('mercury_editor.settings') + ['mapping' => []];
    $config_schema = $config_schema['mapping']['inline_editing']['mapping'];

    $form['placeholder_token'] = [
      '#type' => 'textfield',
      '#title' => $config_schema['placeholder_token']['label'],
      '#description' => $config_schema['placeholder_token']['description'],
      '#default_value' => $config->get('inline_editing.placeholder_token') ?? $this->t('<!-- mercury placeholder -->'),
    ];
    $form['placeholder_text'] = [
      '#type' => 'textfield',
      '#title' => $config_schema['placeholder_text']['label'],
      '#description' => $config_schema['placeholder_text']['description'],
      '#default_value' => $config->get('inline_editing.placeholder_text') ?? $this->t('Write something...'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('mercury_editor.settings');
    $config->set('inline_editing.placeholder_token', $form_state->getValue('placeholder_token'));
    $config->set('inline_editing.placeholder_text', $form_state->getValue('placeholder_text'));
    $config->save();
    // Confirmation on form submission.
    $this->messenger()->addMessage($this->t('The Mercury Editor settings have been saved.'));
  }

}
