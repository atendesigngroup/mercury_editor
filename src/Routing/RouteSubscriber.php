<?php

namespace Drupal\mercury_editor\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('layout_paragraphs.builder.insert')) {
      $route->setDefault('_controller', '\Drupal\mercury_editor\Controller\InsertComponentController::skipInsertForm');
    }
  }

}
