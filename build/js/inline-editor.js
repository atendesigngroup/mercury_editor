(function () {
  'use strict';

  (($, Drupal, drupalSettings, debounce) => {

    function getDrupalFormat(format_id) {
      return drupalSettings.editor.formats[format_id];
    }

    function getDrupalEditor(format_id) {
      const format = getDrupalFormat(format_id);
      const editor = Drupal.editors[format.editor];
      return editor;
    }

    function getFieldParameters(element) {
      const $field = $(element).closest('[data-me-field_name]');
      return {
        fieldName: $field.attr('data-me-field_name'),
        viewMode: $field.attr('data-me-view_mode'),
        formatId: $field.attr('data-me-format_id'),
        uuid: $field.closest('[data-uuid]').attr('data-uuid'),
        layoutId: $field.closest('[data-lpb-id]').attr('data-lpb-id'),
      };
    }

    function getRawFieldValue(uuid, fieldName, viewMode) {
      return drupalSettings.mercuryEditor.inlineEditor.rawValues[uuid][fieldName][viewMode];
    }

    function saveField(lpbUuid, uuid, fieldName, formatId, data) {
      Drupal.ajax({
        url: `${drupalSettings.path.baseUrl}${drupalSettings.path.pathPrefix}mercury-editor/${lpbUuid}/save-component/${uuid}`,
        submit: {
          field_name: fieldName,
          format_id: formatId,
          data,
        },
      }).execute();
    }

    function attachEditor(element) {
      if (element.classList.contains('me-editor-attached')) {
        return;
      }
      const {fieldName, formatId, viewMode, uuid, layoutId } = getFieldParameters(element);
      const format = getDrupalFormat(formatId);
      const editor = getDrupalEditor(formatId);
      if (typeof editor.meAttachInlineEditor !== 'undefined') {
        getRawFieldValue(uuid, fieldName, viewMode);
        element.innerHTML = getRawFieldValue(uuid, fieldName, viewMode);
        editor.meAttachInlineEditor(element, format, () => {});
        const textElement = editor.meGetTextElement(element);
        editor.onChange(textElement, function (htmlText) {
          debounce(saveField, 250)(layoutId, uuid, fieldName, formatId, htmlText);
        });
        element.classList.add('me-editor-attached');
      }
    }

    function focusEditor(element) {
      const { formatId } = getFieldParameters(element);
      const editor = getDrupalEditor(formatId);
      if (typeof editor.meFocusEditor !== 'undefined') {
        editor.meFocusEditor(element);
      }
    }

    Drupal.behaviors.layoutParagraphsInlineEditor = {
      attach: (context) => {
        $('[data-lpb-form-id] .me-is-editable').once('me-inline-editor').each((i, e) => {
          attachEditor(e);
        });
        if (
          context.attributes
          && context.getAttribute('data-uuid')
          && $('.me-is-editable', context).length > 0
        ) {
          focusEditor($('.me-is-editable', context)[0]);
        }
      },
    };

  })(jQuery, Drupal, drupalSettings, Drupal.debounce);

}());
