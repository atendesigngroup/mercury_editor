(function () {
  'use strict';

  (($, Drupal, CKEDITOR, debounce) => {

    Drupal.editors.ckeditor = {
      ...Drupal.editors.ckeditor,
      meAttachInlineEditor(element, format) {
        if (element.getAttribute('data-placeholder')) {
          format.editorSettings.editorplaceholder = element.getAttribute('data-placeholder');
        }
        this.attachInlineEditor(element, format);
        const instance = CKEDITOR.dom.element.get(element).getEditor();
        instance.on('focus', (e) => {
          $(element).closest('[data-uuid]').attr('data-is-live-editing', true);
        });
        instance.on('blur', (e) => {
          $(element).closest('[data-uuid]').removeAttr('data-is-live-editing');
        });
      },
      meFocusEditor(element) {
        $(element).focus();
      },
      meGetTextElement(element) {
        return element;
      }
    };

  })(jQuery, Drupal, CKEDITOR, Drupal.debounce);

}());
