const postcssPresetEnv = require('postcss-preset-env');
const pxtorem = require('postcss-pxtorem');

module.exports = {
  plugins: [
    pxtorem({
      propList: ['--font*', 'font', 'font*'],
    }),
    postcssPresetEnv({
      stage: 1,
      features: {
        // Custom properties get poyfilled for IE so no need to process them.
        'custom-properties': false,
      },
    }),
  ],
};
