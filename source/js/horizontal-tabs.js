(($) => {
  /**
   * Repositions horizontal tabs into the header of a modal dialog.
   */
  $(window).on('dialog:aftercreate', (event, dialog, $dialog) => {
    if ($dialog.attr('id').indexOf('lpb-dialog-') === 0) {
      const $tabs = $dialog.find('.horizontal-tab-radios');
      const $titlebar = $dialog.closest('.ui-dialog').find('.ui-dialog-titlebar');
      if ($tabs.length && $titlebar.length) {
        $titlebar
          .addClass('has-tabs')
          .append($tabs);
      }
    }
  });

})(jQuery);
